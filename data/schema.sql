CREATE TABLE
    agency (
        agency_id INT NOT NULL,
        agency_name TEXT NOT NULL,
        agency_url TEXT NOT NULL,
        agency_timezone TEXT NOT NULL,
        agency_lang TEXT NOT NULL,
        PRIMARY KEY (agency_id)
    );

CREATE TABLE
    calendar_dates (
        service_id INT,
        date TEXT,
        exception_type INT
    );
CREATE UNIQUE INDEX calendar_dates__by_service_id
    ON calendar_dates (service_id, date);
CREATE UNIQUE INDEX calendar_dates__by_date
    ON calendar_dates (date, service_id);

CREATE TABLE
    stops (
        stop_id INT NOT NULL,
        stop_name TEXT NOT NULL,
        stop_lat REAL NOT NULL,
        stop_lon REAL NOT NULL,
        location_type INT NOT NULL,
        parent_station INT,
        PRIMARY KEY (stop_id),
        FOREIGN KEY (parent_station) REFERENCES stops (location_type)
    );
CREATE INDEX stops__parent_station
    ON stops (parent_station);

CREATE TABLE
    routes (
        route_id INT NOT NULL,
        agency_id INT NOT NULL,
        route_short_name TEXT NOT NULL,
        route_long_name TEXT NOT NULL,
        route_type INT NOT NULL,
        route_color TEXT,
        route_text_color TEXT,
        PRIMARY KEY (route_id),
        FOREIGN KEY (agency_id) REFERENCES agency (agency_id)
    );
CREATE INDEX routes__by_agency_id
    ON routes (agency_id);

CREATE TABLE
    trips (
        route_id INT NOT NULL,
        service_id INT NOT NULL,
        trip_id INT NOT NULL,
        trip_headsign TEXT NOT NULL,
        direction_id INT,
        block_id INT NOT NULL,
        PRIMARY KEY (trip_id),
        FOREIGN KEY (route_id) REFERENCES routes (route_id),
        FOREIGN KEY (service_id) REFERENCES calendar_dates (service_id)
    );
CREATE INDEX trips__by_route_id
    ON trips (route_id);
CREATE INDEX trips__by_service_id
    ON trips (service_id);

CREATE TABLE
    stop_times (
        trip_id INT NOT NULL,
        arrival_time TEXT NOT NULL,
        departure_time TEXT NOT NULL,
        stop_id INT NOT NULL,
        stop_sequence INT NOT NULL,
        pickup_type INT NOT NULL,
        drop_off_type INT NOT NULL,
        PRIMARY KEY (trip_id, stop_sequence),
        FOREIGN KEY (trip_id) REFERENCES trips (trip_id),
        FOREIGN KEY (stop_id) REFERENCES stops (stop_id)
    );
CREATE INDEX stop_times__by_trip_id
    ON stop_times (trip_id);
CREATE INDEX stop_times__by_stop_id
    ON stop_times (stop_id);

CREATE TABLE
    feed_info(
        feed_id INT NOT NULL,
        feed_publisher_name TEXT NOT NULL,
        feed_publisher_url  TEXT NOT NULL,
        feed_lang TEXT NOT NULL,
        feed_start_date TEXT NOT NULL,
        feed_end_date TEXT NOT NULL,
        feed_version TEXT NOT NULL,
        PRIMARY KEY (feed_id)
    );