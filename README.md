# TER

Interface web exploitant le dataset [sncf-ter-gtfs@datasncf](https://data.opendatasoft.com/explore/dataset/sncf-ter-gtfs%40datasncf/information/) (horaires des lignes TER).

Pour le moment c'est juste une excuse pour découvrir [htmx](https://htmx.org/).

Toutes propositions de fonctionnalité sont les bienvenues !

## Développement

### Installer le package en édition
`$ pip install -e .[dev]`

### Mettre a jour la base de données
Le fichier de données [GTFS](https://gtfs.org/schedule/reference/) est mis à
jour quotidiennement par le SNCF. Pour travailler sur les dernières données
disponibles:

`$ python3 data/update.py`

### Recompilation continue du fichier `css`
`$ tailwindcss -i ter/static/src/tw.css  -o ter/static/css/main.css --watch`

### Démarrage du serveur de développement
`$ uvicorn ter.main:app --reload`

### Lancer la suite de tests
`$ pytest`

## Notes
Les fichiers de code (`.py`, `.sql`) et les `commits` sont écrits (et documentés)
en anglais.
