import os.path as path

from fastapi.responses import HTMLResponse
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    APP_DIR: str = path.dirname(__file__)

    STATIC_DIR: str = path.join(APP_DIR, "static")
    TEMPLATE_DIR: str = path.join(APP_DIR, "templates")
    DATA_DIR: str = path.join(path.dirname(APP_DIR), "data")

    SQLITE_URI: str = f"file:{path.join(DATA_DIR, 'db.sqlite')}?mode=ro"

    FASTAPI_PROPERTIES: dict = {
        "title": "TER",
        "description": "",
        "version": "0.0.1",
        "default_response_class": HTMLResponse,  # Change default from JSONResponse
        "openapi_url": None,
        "openapi_prefix": None,
        "docs_url": None,
        "redoc_url": None,
    }


settings = Settings()
