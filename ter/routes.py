from typing import Annotated

from fastapi import APIRouter, Request, Header
from jinja2_fragments.fastapi import Jinja2Blocks


from ter.config import Settings
from ter.helpers import database

settings = Settings()
router = APIRouter()
templates = Jinja2Blocks(settings.TEMPLATE_DIR)


@router.get("/station")
async def station(
    request: Request,
    search: str | None = None,
    name: str | None = None,
    hx_target: Annotated[str | None, Header()] = None,
):
    context = {"request": request}

    if name:
        context["name"] = name
        cursor = await database.execute(
            "SELECT stop_lat, stop_lon FROM stops WHERE location_type=1 and stop_name = ?",
            (name,),
        )
        context["location"] = await cursor.fetchone()

    elif search:
        if len(search) > 2:
            cursor = await database.execute(
                "SELECT stop_name FROM stops WHERE location_type=1 and stop_name LIKE ?",
                (f"%{search}%",),
            )
            context["search_results"] = await cursor.fetchall()
        context["search"] = search

    if hx_target:
        hx_target = hx_target.replace("-", "_")

    return templates.TemplateResponse("station.html", context, block_name=hx_target)


@router.get("/")
async def index(request: Request):
    """Home page."""

    cursor = await database.execute("SELECT * FROM agency")
    agencies = await cursor.fetchall()

    context = {"request": request, "agencies": agencies}

    return templates.TemplateResponse("index.html", context)
