import contextlib as ctx

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from ter.config import Settings
from ter.helpers import database
from ter.routes import router

settings = Settings()


@ctx.asynccontextmanager
async def lifespan(app: FastAPI):
    await database.connect()
    yield
    await database.disconnect()


def get_app() -> FastAPI:
    app = FastAPI(
        **settings.FASTAPI_PROPERTIES,
        lifespan=lifespan,
    )
    app.mount(
        "/static",
        StaticFiles(directory=settings.STATIC_DIR),
        name="static",
    )
    app.include_router(router)

    return app


app = get_app()
